//console.log("Zawarudo")

// [Section] Document Object Model (DOM)

	// allows us to access or modify the properties of an html element in a webpage.
	// it is the standard on how to get, change, add, or delete HTML elemets.
	// we will focus on use of DOM in managing forms.

	// for selecting HTML elements we will be using 'document.querySelector'
	// the query selector function takes a string input that is formatted like a css selector when applying the styles

		// syntax: document.querySelector("html element")


		const textFirstName = document.querySelector("#txt-frist-name");
		const textLastName = document.querySelector("#txt-last-name");
		const color = document.querySelector("#colors");

		// console.log(textFirstName);

		// const name = document.querySelectorAll(".full-name");
		// console.log(name);

		// const span = document.querySelectorAll("span");
		// console.log(span)

		// const text = document.querySelectorAll("input[type]");
		// console.log(text);

		// const div = document.querySelectorAll(".first-div>span"); //span.divClass/id
		// console.log(div);

		const spanFullName = document.querySelector("#fullName");


// [Section] Event Listener
	// when a user interacts with a webpage, this action is considered as an event.
	// working with events is large part of creating interactive in a webpage
	// specific functions that perform an action

	// "addEventListener" is the function used that takes two arguments.
		// first argument is a string indentifying an event
		// second argument, function that the listener will trigger once the "specified event" is triggered.

		// textFirstName.addEventListener("keyup", (event) => {
		// 	console.log(event.target.value);
		// })

		const fullName = () => {
			spanFullName.innerHTML = `${textFirstName.value} ${textLastName.value}`
		}

		textFirstName.addEventListener("keyup", fullName)
		textLastName.addEventListener("keyup", fullName)

		
		const changeColor = () => {
			let colors = document.getElementById("color").value;
			document.getElementById("fullName").style.color = colors;
		}